//
//  CellConvite.swift
//  Partify
//
//  Created by Thiago Vinhote on 04/12/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//
import Foundation
import WatchKit
import WatchConnectivity

class CellConvite: NSObject {

    weak var session: WCSession? {
        return (WKExtension.shared().delegate as? ExtensionDelegate)?.session
    }
    
    @IBOutlet var group: WKInterfaceGroup!
    weak var convite: Convite? {
        didSet{
            self.setup()
        }
    }

    @IBOutlet var nameFrom: WKInterfaceLabel!
    @IBOutlet var nameParty: WKInterfaceLabel!
    
    private func setup() {
//        self.group.setBackgroundImageNamed("Activity")
//        self.group.startAnimating()
        if let idUser = self.convite?.fromId {
            session?.sendMessage(["comando": "pedirUser", "valor": idUser], replyHandler: { (response: [String : Any]) in
                if let user = response["user"] as? String {
                    self.nameFrom.setText(user)
                }
//                self.group.stopAnimated()
            }, errorHandler: { (error: Error) in
                print("watchOS: " + error.localizedDescription)
            })
        }
        if let idFesta = self.convite?.idFesta {
            session?.sendMessage(["comando": "pedirFesta", "valor": idFesta], replyHandler: { (response: [String : Any]) in
                if let festa = response["festa"] as? String {
                    self.nameParty.setText("\"\(festa)\"")
                }
//                self.group.stopAnimated()
            }, errorHandler: { (error: Error) in
                print("watchOS: " + error.localizedDescription)
            })
        }
            
    }

}
