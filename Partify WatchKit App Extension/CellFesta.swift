//
//  CellFesta.swift
//  Partify
//
//  Created by Thiago Vinhote on 29/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import WatchKit
import WatchConnectivity

class CellFesta: NSObject {

    @IBOutlet var imageFesta: WKInterfaceImage!
    @IBOutlet var nomeFesta: WKInterfaceLabel!
    @IBOutlet var localFesta: WKInterfaceLabel!
    @IBOutlet var dataFesta: WKInterfaceLabel!
    
    weak var session: WCSession? {
        return (WKExtension.shared().delegate as? ExtensionDelegate)?.session
    }
    
    weak var festa: Festa? {
        didSet{
            if let nome = self.festa?.nome {
                self.nomeFesta.setText(nome)
            }
            if let local = self.festa?.local {
                self.localFesta.setText(local)
            }
            if let dataFesta = self.festa?.data {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy, HH:mm"
                let date = Date(timeIntervalSince1970: TimeInterval(dataFesta))
                self.dataFesta.setText(dateFormatter.string(from: date))
            }
            if let imageUrl = self.festa?.imageProfileUrl {
                session?.sendMessage(["comando": "pedirImage", "valor": imageUrl], replyHandler: { (response: [String : Any]) in
                    if let imageData = response["image"] as? Data, let image = UIImage(data: imageData){
                        self.imageFesta.setImage(image)
                        
                    }
                }, errorHandler: { (error: Error) in
                    print("watchOS: " + error.localizedDescription)
                })
            }
        }
    }
}
