//
//  ConvitesInterfaceController.swift
//  Partify
//
//  Created by Thiago Vinhote on 02/12/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import WatchKit
import WatchConnectivity

class ConvitesInterfaceController: WKInterfaceController {

    var convites: [Convite] = []

    weak var session: WCSession? {
        return (WKExtension.shared().delegate as? ExtensionDelegate)?.session
    }
    @IBOutlet var groupIndicator: WKInterfaceGroup!

    @IBOutlet var tableConvite: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        self.groupIndicator.setBackgroundImageNamed("Activity")
        self.groupIndicator.startAnimating()
        session?.sendMessage(["comando": "pedirConvites"], replyHandler: { (response: [String : Any]) in
            print("watchOS: Recibi uma resposta")
            if let convites =  response["convites"] as? [NSDictionary] {
                print(convites)
                self.convites = convites.map({ (dic: NSDictionary) -> Convite in
                    let c = Convite(dic: dic as! [String: Any])
                    return c
                })
                self.groupIndicator.stopAnimated()
                self.setupTable()
            }
        }, errorHandler: { (Error) in
            print("watchOS: Houve um erro de comunicação", Error)
        })
    }
    
    override func contextForSegue(withIdentifier segueIdentifier: String, in table: WKInterfaceTable, rowIndex: Int) -> Any? {
        if segueIdentifier == "cellItemConvite" {
            let row = table.rowController(at: rowIndex) as! CellConvite
            return ["convite": self.convites[rowIndex], "indexTable": rowIndex, "controller": self]
        }
        return nil
    }

    override func didAppear() {
        super.didAppear()
    }
    
    internal func removeItem(index: Int) {
        self.convites.remove(at: index)
        self.setupTable()
    }
    
    private func setupTable() {
        self.tableConvite.setNumberOfRows(self.convites.count, withRowType: "cellConvite")
        for (index, item) in self.convites.enumerated() {
            let row = self.tableConvite.rowController(at: index) as! CellConvite
            row.convite = item
        }
    }
    
}
