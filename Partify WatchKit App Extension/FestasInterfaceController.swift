//
//  FestarInterfaceController.swift
//  Partify
//
//  Created by Thiago Vinhote on 29/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import WatchKit
import WatchConnectivity

extension WKInterfaceGroup {
    
    func stopAnimated() {
        self.stopAnimating()
        self.setHidden(true)
    }
}

class FestasInterfaceController: WKInterfaceController {

    var festas: [Festa] = []
    
    @IBOutlet var gruopIndicator: WKInterfaceGroup!
    
    weak var session: WCSession? {
        return (WKExtension.shared().delegate as? ExtensionDelegate)?.session
    }
    
    @IBOutlet var tableFestas: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        self.gruopIndicator.setBackgroundImageNamed("Activity")
        self.gruopIndicator.startAnimating()
    }
    
    override func didAppear() {
        super.didAppear()
        session?.sendMessage(["comando": "pedirFestas"], replyHandler: { (response: [String : Any]) in
            print("watchOS: Recibi uma resposta")
            print(response)
            if let festas =  response["festas"] as? [NSDictionary] {
                self.festas = festas.map({ (dic: NSDictionary) -> Festa in
                    let f = Festa(dic: dic as! [String: Any])
                    return f
                })
                self.gruopIndicator.stopAnimated()
                print(festas, self.festas)
                self.setupTable()
            }
        }, errorHandler: { (Error) in
            print("watchOS: Houve um erro de comunicação", Error)
        })
    }
    
    override func contextForSegue(withIdentifier segueIdentifier: String, in table: WKInterfaceTable, rowIndex: Int) -> Any? {
        if segueIdentifier == "cellItem" {
            return ["idFesta": self.festas[rowIndex].id]
        }
        return nil
    }
    
    private func setupTable() {
        self.tableFestas.setNumberOfRows(self.festas.count, withRowType: "cellid")
        for (index, item) in self.festas.enumerated() {
            let row = self.tableFestas.rowController(at: index) as! CellFesta
            row.festa = item
        }
    }
}
