//
//  InterfaceController.swift
//  Partify WatchKit App Extension
//
//  Created by Thiago Vinhote on 25/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import WatchKit
import Foundation

class HomeInterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    override func didAppear() {
        super.didAppear()
    }
}
