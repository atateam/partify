//
//  ItemFestaController.swift
//  Partify
//
//  Created by Thiago Vinhote on 04/12/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import WatchKit
import WatchConnectivity

class ItemFestaController: WKInterfaceController {

    var itens: [String] = []
    var idFesta: String?
    
    @IBOutlet var groupIndicator: WKInterfaceGroup!
    
    weak var session: WCSession? {
        return (WKExtension.shared().delegate as? ExtensionDelegate)?.session
    }
    
    @IBOutlet weak var tableItens: WKInterfaceTable!

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        self.groupIndicator.setBackgroundImageNamed("Activity")
        self.groupIndicator.startAnimating()
        self.idFesta = (context as? NSDictionary)?.object(forKey: "idFesta") as? String
        self.fetchItens()
    }
    
    private func fetchItens() {
        if let idFesta = self.idFesta {
            session?.sendMessage(["comando": "pedirItens", "valor": idFesta], replyHandler: { (response: [String : Any]) in
                if let itens = response["itens"] as? [String] {
                    self.itens = itens
                }
                self.setupTable()
                self.groupIndicator.stopAnimated()
            }, errorHandler: { (error: Error) in
                print("watchOS: " + error.localizedDescription)
            })
        }
    }
    
    private func setupTable() {
        self.tableItens.setNumberOfRows(self.itens.count, withRowType: "cellitem")
        for (index, item) in self.itens.enumerated() {
            let row = self.tableItens.rowController(at: index) as! CellItem
            row.nomeItem.setText(item)
        }
    }
    
}
