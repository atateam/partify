//
//  ModalConviteController.swift
//  Partify
//
//  Created by Thiago Vinhote on 04/12/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import WatchKit
import WatchConnectivity

class ModalConviteController: WKInterfaceController {

    weak var session: WCSession? {
        return (WKExtension.shared().delegate as? ExtensionDelegate)?.session
    }

    @IBOutlet var nomeUser: WKInterfaceLabel!
    @IBOutlet var nomeParty: WKInterfaceLabel!
    var index: Int?
    weak var controlller: ConvitesInterfaceController?
    
    @IBOutlet var groupIndicator: WKInterfaceGroup!
    weak var convite: Convite? {
        didSet{
            self.setup()
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        self.groupIndicator.setBackgroundImageNamed("Activity")
        self.groupIndicator.startAnimating()
        self.convite = (context as? NSDictionary)?.object(forKey: "convite") as? Convite
        self.index =  (context as? NSDictionary)?.object(forKey: "indexTable") as? Int
        self.controlller = (context as? NSDictionary)?.object(forKey: "controller") as? ConvitesInterfaceController
    }
    
    @IBAction func handleRecusar() {
        self.respostaConvite(false)
        self.dismiss()
        if let i = index {
            self.controlller?.removeItem(index: i)
        }
    }
    
    @IBAction func handleConfirmar() {
        self.respostaConvite(true)
        self.dismiss()
        if let i = index {
            self.controlller?.removeItem(index: i)
        }
    }
    
    private func respostaConvite(_ status: Bool) {
        let conviteDic : [String: Any] = ["id": convite?.id, "idFesta": convite?.idFesta, "toId": convite?.toId]
        
        self.session?.sendMessage(["comando": "respostaPedido", "valor": status, "dic": conviteDic], replyHandler: { (response: [String : Any]) in
            if let status = response["status"] as? Bool {
                print(status)
            } else {
                print("Error na resposta")
            }
        }, errorHandler: { (error: Error) in
            print(error)
        })
    }
    
    private func setup() {
        if let idUser = self.convite?.fromId {
            session?.sendMessage(["comando": "pedirUser", "valor": idUser], replyHandler: { (response: [String : Any]) in
                if let user = response["user"] as? String {
                    self.nomeUser.setText(user)
                }
                self.groupIndicator.stopAnimated()
            }, errorHandler: { (error: Error) in
                print("watchOS: " + error.localizedDescription)
            })
        }
        if let idFesta = self.convite?.idFesta {
            session?.sendMessage(["comando": "pedirFesta", "valor": idFesta], replyHandler: { (response: [String : Any]) in
                if let festa = response["festa"] as? String {
                    self.nomeParty.setText("\"\(festa)\"")
                }
                self.groupIndicator.stopAnimated()
            }, errorHandler: { (error: Error) in
                print("watchOS: " + error.localizedDescription)
            })
        }
        
    }
    
}
