//
//  AppDelegate.swift
//  Partify
//
//  Created by Thiago Vinhote on 10/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FBSDKCoreKit
import FirebaseMessaging
import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var session: WCSession? {
        didSet{
            if let session = session {
                session.delegate = self
                session.activate()
            }
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        application.statusBarStyle = .lightContent
        FIRApp.configure()
        FIRDatabase.database().persistenceEnabled = true
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        if WCSession.isSupported() {
            self.session = WCSession.default()
        }

        self.setup()
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                                         selector: #selector(self.tokenRefreshNotification),
                                                         name: NSNotification.Name.firInstanceIDTokenRefresh,
                                                         object: nil)
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func setup() {
        let gradient = CAGradientLayer()
        let sizeLength = UIScreen.main.bounds.size.height * 2
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
        gradient.frame = defaultNavigationBarFrame
        gradient.colors = [UIColor.prtLightIndigoTwo.cgColor, UIColor.prtAmethyst.cgColor]
        UINavigationBar.appearance().setBackgroundImage(UIImage.image(fromLayer: gradient), for: .default)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // Print message ID.
        print("Message ID: \(userInfo["gcm.message_id"])")
        // Print full message.
        print(userInfo)
        print("Push notification received: \(userInfo)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // Let FCM know about the message for analytics etc.
        FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        // handle your message
        print("Push notification received: \(userInfo)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.unknown)
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        // Persist it in your backend in case it's new
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        connectToFcm()
    }
    
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        print("Message ID: \(userInfo)")
        // Print full message.
        print("%@", userInfo)
    }
}

extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print("%@", remoteMessage.appData)
    }
}

extension AppDelegate: WCSessionDelegate {

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("IOS: session is active.")
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        print("iOS: Mensagem recebida: \(message)")
        if let comando = message["comando"] as? String {
            switch comando {
            case "pedirFestas":
                var festas: [NSDictionary] = []
                FestaStore.singleton.fetchFestasWatch(completion: { (fs: [Festa]) in
                    print(festas)
                    festas = fs.map({ (f: Festa) -> NSDictionary in
                        return ["nome": f.nome, "local": f.local, "data": f.data, "imageProfileUrl": f.imageProfileUrl, "id": f.id]
                    })
                    replyHandler(["festas": festas])
                })
                break
            case "pedirImage":
                if let imageUrl = message["valor"] as? String {
                    UIImage.loadImage(withUrl: imageUrl, completion: { (imagem: UIImage?) in
                        if let imagem = imagem, let imageData = UIImageJPEGRepresentation(imagem, 0.1) {
                            replyHandler(["image":imageData])
                        }
                    })
                }
                break
            case "pedirItens":
                if let idFesta = message["valor"] as? String {
                    ItemStore.singleton.fetchItemWatch(withId: idFesta, completion: { (itens: [String]) in
                        replyHandler(["itens": itens])
                    })
                }
                break
            case "pedirUser":
                if let idUser = message["valor"] as? String {
                    UserStore.singleton.fetchUser(withId: idUser, completion: { (user: User?) in
                        if let nome = user?.nome {
                            replyHandler(["user": nome])
                        }
                    })
                }
            case "pedirFesta":
                if let idFesta = message["valor"] as? String {
                    FestaStore.singleton.fetchFesta(withId: idFesta, completion: { (festa: Festa?) in
                        if let nome = festa?.nome {
                            replyHandler(["festa": nome])
                        }
                    })
                }
                break
            case "pedirConvites":
                ConviteStore.singleton.fetchConvitesWatch(completion: { (convites: [Convite]) in
                    let dic = convites.map({ (convite: Convite) -> NSDictionary in
                        return ["id": convite.id, "idFesta": convite.idFesta, "fromId": convite.fromId, "toId": convite.toId]
                    })
                    replyHandler(["convites": dic])
                })
                break
            case "respostaPedido":
                if let status = message["valor"] as? Bool, let dic = message["dic"] as? [String: Any] {
                    let convite = Convite(dic: dic)
                    if status {
                        ConviteStore.singleton.confirmaConvite(convite, completion: { (convite: Convite?, error: Error?) in
                            if error != nil {
                                replyHandler(["status": false])
                            } else {
                                replyHandler(["status": true])
                            }
                        })
                    } else {
                        ConviteStore.singleton.recusarConvite(convite, completion: { (convite: Convite?, error: Error?) in
                            if error != nil {
                                replyHandler(["status": false])
                            } else {
                                replyHandler(["status": true])
                            }
                        })
                    }
                }
                break
            default:
                break
            }
        }
    }
    
    func send(){
        session?.sendMessage(["newMessage": "newImage"], replyHandler: { (response: [String : Any]) in
            print("iOS: Enviei uma mensagem")
            
        }, errorHandler: { (Error) in
            print("iOS: Houve um erro de comunicação")
        })
    }
    
}
