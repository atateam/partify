//
//  CellConvite.swift
//  Partify
//
//  Created by Thiago Vinhote on 22/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class CellConvite: UICollectionViewCell {
    
    @IBOutlet weak var imageConvite: UIImageView!
    @IBOutlet weak var buttonRecusar: UIButton!
    @IBOutlet weak var labelMensagem: UILabel!

    @IBOutlet weak var labelMessagemWidthConstraint: NSLayoutConstraint!
    
    var convite: Convite? {
        didSet{
            self.setup()
        }
    }
    
    let fontSemibold = [NSFontAttributeName : UIFont.systemFont(ofSize: 15, weight: UIFontWeightSemibold), NSForegroundColorAttributeName : UIColor.prtBlueberry]
    let fontRegular = [NSFontAttributeName : UIFont.systemFont(ofSize: 15, weight: UIFontWeightRegular), NSForegroundColorAttributeName : UIColor.prtBlueberry]
    var attrInvited : NSMutableAttributedString {
        return NSMutableAttributedString(string: " invited you to ", attributes: self.fontRegular)
    }
    
    var attributedString1: NSMutableAttributedString?
    
    private func setup(){
        self.buttonRecusar.layer.borderColor = UIColor.prtLightIndigo.cgColor
        self.buttonRecusar.layer.borderWidth = 1.0
        if let convite = self.convite {
            if let fromId = self.convite?.fromId {
                UserStore.singleton.fetchUser(withId: fromId, completion: { (user: User?) in
                    if let nome = user?.nome {
                        self.attributedString1 = NSMutableAttributedString(string: nome, attributes: self.fontSemibold)
                        self.attributedString1?.append(self.attrInvited)
                        self.labelMensagem.attributedText = self.attributedString1
                    }
                })
            }
            if let idFesta = convite.idFesta {
                FestaStore.singleton.fetchFesta(withId: idFesta, completion: { (festa: Festa?) in
                    if let url = festa?.imageProfileUrl {
                        self.imageConvite.loadImageUsingCache(withUrlString: url)
                    }
                    if let nomeParty = festa?.nome {
                        let attributedString2 = NSMutableAttributedString(string: "\"\(nomeParty)\"", attributes: self.fontSemibold)
                        self.attributedString1?.append(attributedString2)
                        self.labelMensagem.attributedText = self.attributedString1
                        
                        let texto = self.attributedString1?.string
                        if let height = texto?.rect(view: self.labelMensagem, sizeFont: 15).height {
                            self.labelMessagemWidthConstraint.constant = height + 8
                        }
                    }
                })
            }
        }
    }
    
    @IBAction func handleRecusar(_ sender: Any) {
        if let convite = self.convite {
            convite.status = false
            ConviteStore.singleton.recusarConvite(convite, completion: { (convite: Convite?, error: Error?) in
                self.convite = convite
            })
        }
    }
    
    @IBAction func handleConfirmar() {
        if let convite = self.convite {
            convite.status = true
            ConviteStore.singleton.confirmaConvite(convite, completion: { (convite: Convite?, error: Error?) in
                self.convite = convite
            })
        }
    }
}
