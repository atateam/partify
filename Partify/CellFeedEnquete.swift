//
//  CellFeedEnquete.swift
//  Partify
//
//  Created by Arthur Melo on 17/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class CellFeedEnquete: UICollectionViewCell {
    @IBOutlet weak var surveyName: UILabel!
    @IBOutlet weak var surveyQuestion: UILabel!
    @IBOutlet weak var surveyDate: UILabel!
    
    weak var enquete: Enquete? {
        didSet {
            self.setupViews()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews() {
        if let nome = self.enquete?.nome {
            self.surveyName.text = nome
        }
        if let data = self.enquete?.data {
            let date = Date(timeIntervalSince1970: TimeInterval(data))
            let formate = DateFormatter()
            formate.dateFormat = "dd-MM HH:mm"
            self.surveyDate.text = formate.string(from: date)
        }
        if let pergunta = self.enquete?.pergunta {
            self.surveyQuestion.text = pergunta
        }
    }
}
