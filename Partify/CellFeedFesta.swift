//
//  CellFeedFesta.swift
//  Partify
//
//  Created by Thiago Vinhote on 12/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class CellFeedFesta: UICollectionViewCell {
    
    @IBOutlet weak var labelNome: UILabel!
    @IBOutlet weak var labelData: UILabel!
    @IBOutlet weak var labelLocal: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    
    weak var festa: Festa? {
        didSet{
            self.setupViews()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func handleDelete() {
        let alertControll : UIAlertController = {
            let ac = UIAlertController(title: "Remover festa", message: "Você quer sair dessa festa?", preferredStyle: .actionSheet)
            let actionRemove = UIAlertAction(title: "Remover", style: .destructive, handler: { (action: UIAlertAction) in
                if let festa = self.festa {
                    FestaStore.singleton.removeUserFesta(festa, completion: { (festa: Festa?, error: Error?) in
                        if let e = error {
                            print(e)
                            return
                        }
                    })
                }
            })
            let actionCancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            ac.addAction(actionCancel)
            ac.addAction(actionRemove)
            return ac
        }()
        self.window?.rootViewController?.present(alertControll, animated: true, completion: nil)
    }
    
    func setupViews() {
        self.backgroundView = UIView()
        self.backgroundView?.setGradient(colors: [UIColor.prtLightIndigoTwo, UIColor.prtAmethyst])
        self.backgroundView?.addMaskWhite(white: 1.0, alpha: 0.3)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.bounds = self.bounds
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 3
        
        if let nome = self.festa?.nome {
            self.labelNome.text = nome
        }
        if let data = self.festa?.data {
            let date = Date(timeIntervalSince1970: TimeInterval(data))
            let formate = DateFormatter()
            formate.dateFormat = "dd-MM-yyyy, HH:mm"
            self.labelData.text = formate.string(from: date)
        }
        if let local = self.festa?.local {
            self.labelLocal.text = local
        }
        if let imageUrl = self.festa?.imageProfileUrl {
            self.imageProfile.loadImageUsingCache(withUrlString: imageUrl)
        } else {
            self.imageProfile.image = nil
        }
        
        self.imageProfile.layer.cornerRadius = self.imageProfile.frame.height / 2
        self.imageProfile.layer.masksToBounds = true
    }
}
