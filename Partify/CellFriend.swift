//
//  CellFriend.swift
//  Partify
//
//  Created by Thiago Vinhote on 21/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class CellFriend: UICollectionViewCell {
    
    @IBOutlet weak var imageProfile: UIImageView!
    
    weak var friend: Friend? {
        didSet{
            self.setup()
        }
    }
    
    weak var user: User? {
        didSet{
            self.setupUser()
        }
    }
    
    func setup() {
        if let url = self.friend?.urlImageProfile {
            self.imageProfile.loadImageUsingCache(withUrlString: url)
        }
    }
    
    func setupUser() {
        if let url = self.user?.imageProfileUrl {
            self.imageProfile.loadImageUsingCache(withUrlString: url)
        }
    }
}
