//
//  CellFriendTable.swift
//  Partify
//
//  Created by Thiago Vinhote on 24/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class CellFriendTable: UITableViewCell {

    weak var friend: Friend? {
        didSet{
            self.setup()
        }
    }
    
    weak var user: User? {
        didSet{
            self.setupUser()
        }
    }
    
    private func setupUser() {
        if let nome = self.user?.nome {
            self.nameFriend.text = nome
        }
        if let imageUrl = self.user?.imageProfileUrl {
            self.photoFriend.loadImageUsingCache(withUrlString: imageUrl)
        }
    }
    
    @IBOutlet weak var imageSelected: UIImageView!
    @IBOutlet weak var photoFriend: UIImageView!
    @IBOutlet weak var nameFriend: UILabel!
    
    private func setup() {
        self.accessoryType = .none
        self.selectionStyle = .none
        if let friend = self.friend {
            guard let id = friend.id else {
                return
            }
            UserStore.singleton.graphRequestFacebook(id: id, completion: { (any: Any?, error: Error?) in
                if let d = any as? NSDictionary {
                    friend.setValuesForKeys(d as! [String: Any])
                    if let nome = friend.name {
                        self.nameFriend?.text = nome
                    }
                    if let url = friend.urlImageProfile {
                        self.photoFriend.loadImageUsingCache(withUrlString: url)
                    }
                }
            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
