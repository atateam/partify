//
//  CellItem.swift
//  Partify
//
//  Created by Arthur Melo on 29/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class CellItem: UITableViewCell {

    @IBOutlet weak var itemNome: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionWidthConstraint: NSLayoutConstraint!
    
    let widthCell: CGFloat = 24
    
    var attributedFriends: [User] = []
    weak var item: Item? {
        didSet{
            self.setup()
            let v = UIView()
            v.backgroundColor = UIColor.prtBlueberry
            self.selectedBackgroundView = v
        }
    }
    
    var handleObserverFetch: UInt?
    var handleObserverFetchRemove: UInt?
    
    private func setup() {
        if let nome = self.item?.nome {
            self.itemNome.text = nome
        }
        self.attributedFriends.removeAll()
        self.collectionView.reloadData()
        if let id = self.item?.id {
            self.item?.observerFetch = ItemStore.singleton.fetchItemUser(withId: id, self.item?.observerFetch, completion: { (user: User?, error: Error?) in
                if let e = error {
                    print(e)
                    return
                }
                if let u = user {
                    self.attributedFriends.append(u)
                }
                self.collectionView.reloadData()
            })
            self.item?.observerFetchRemove = ItemStore.singleton.fetchRemovedItemUser(withId: id, self.item?.observerFetchRemove , completion: { (user: User?, error: Error?) in
                if let e = error {
                    print(e)
                    return
                }
                if let u = user {
                    if let index = self.attributedFriends.index(where: { (user: User) -> Bool in
                        return user.id == u.id
                    }) {
                        self.attributedFriends.remove(at: index)
                    }
                }
                self.collectionView.reloadData()
            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        self.collectionView.backgroundColor = .clear
        self.backgroundColor = .clear
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension CellItem: UICollectionViewDelegate {
    
}

extension CellItem: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if attributedFriends.count > 3 {
            return 4
        } else {
            return attributedFriends.count
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CellFriend
        cell.user = attributedFriends[indexPath.item]
        return cell
    }
}

extension CellItem: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return -12
    }
    
}
