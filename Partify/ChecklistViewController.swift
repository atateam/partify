//
//  ChecklistViewController.swift
//  Partify
//
//  Created by Arthur Melo on 18/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class ChecklistViewController: UIViewController {

    var festa: Festa?
    var itens: [Item] = []
    var colours: [UIColor] = [UIColor.blue, UIColor.red, UIColor.yellow, UIColor.green, UIColor.purple]
    var users: [User] = []
    
    @IBOutlet weak var partyImage: UIImageView! {
        didSet {
            partyImage.layer.cornerRadius = partyImage.frame.width / 2
            partyImage.layer.masksToBounds = true
            partyImage.loadImageUsingCache(withUrlString: (festa?.imageProfileUrl)!)
        }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            self.tableView.backgroundColor = .clear
            self.tableView.tableFooterView = UIView()
            self.tableView.tableHeaderView = UIView()
        }
    }
    @IBOutlet weak var partyDescription: UILabel! {
        didSet {
            partyDescription.text = festa?.descricao
        }
    }
    @IBOutlet weak var containerTableView: UIView! {
        didSet{
//            self.containerTableView.setGradient(colors: [.prtAmethyst, .prtLightIndigoTwo])
            self.containerTableView.setShadow(opacity: 0.5)
        }
    }
    @IBOutlet weak var partyDate: UILabel! {
        didSet {
            if let data = self.festa?.data {
                let date = Date(timeIntervalSince1970: TimeInterval(data))
                let formate = DateFormatter()
                formate.dateFormat = "dd-MM-yyyy, HH:mm"
                partyDate.text = formate.string(from: date)
            }
        }
    }
    @IBOutlet weak var partyAddress: UILabel! {
        didSet {
            partyAddress.text = festa?.local
        }
    }
    @IBOutlet weak var showPartyMembers: UIButton! {
        didSet {
            showPartyMembers.layer.cornerRadius = showPartyMembers.frame.height / 2
            showPartyMembers.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var descriptionView: UIView! {
        didSet {
            descriptionView.layer.shadowColor = UIColor.black.cgColor
            descriptionView.layer.shadowRadius = 5
            descriptionView.layer.bounds = descriptionView.bounds
            descriptionView.layer.shadowOpacity = 0.3
            descriptionView.layer.shadowOffset = CGSize.zero
        }
    }
    @IBOutlet weak var itemTextField: UITextField! {
        didSet {
            itemTextField.attributedPlaceholder = NSAttributedString(string: "Ex.: Chocolate cake", attributes: [NSForegroundColorAttributeName: UIColor.white])
        }
    }
    @IBOutlet weak var addItemButton: UIButton! {
        didSet {
            addItemButton.layer.cornerRadius = addItemButton.frame.height / 2
            addItemButton.layer.masksToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationItem.title = festa?.nome
        self.observeUserFestas()
        self.fetchUsers()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewDidLayoutSubviews() {
        itemTextField.addBottomLayer(height: 1.0, color: .white)
    }
    
    private func fetchUsers() {
        if let idFesta = self.festa?.id {
            FestaStore.singleton.fetchFestaUsers(withId: idFesta, completion: { (key: String) in
                UserStore.singleton.fetchUser(withId: key, completion: { (usuario: User?) in
                    if let u = usuario {
                        self.users.append(u)
                        DispatchQueue.main.async {
                            //self.collectionUsers.reloadData()
                        }
                    }
                })
            })
        }
    }
    
    func fetchItem(_ itemId: String) -> Void {
        ItemStore.singleton.fetchItem(withId: itemId, completion: { (item) in
            if let item = item {
                self.itens.append(item)
                self.attemptReloadOfTable()
            }
        })
    }
    
    func observeUserFestas() -> Void {
        if let idFesta = self.festa?.id {
            ItemStore.singleton.fetchItens(festaId: idFesta, completion: { (key) in
                self.fetchItem(key)
            })
        }
    }
    
    var timer: Timer?
    private func attemptReloadOfTable() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    func handleReloadTable() {
        DispatchQueue.main.async {
            print(#function,"Table de itens foi recarregada!")
            self.tableView.reloadData()
        }
    }
    
    @IBAction func addItem(_ sender: UIButton) {
        let item = Item()
        item.nome = itemTextField.text!
        
        ItemStore.singleton.createItem(item) { (item: Item?, error: Error?) in
            if error != nil {
                print("Aconteceu algo de errado ao tentar salvar!", error!)
                return
            }
            if let idFesta = self.festa?.id {
                ItemStore.singleton.createFestaItem(festaId: idFesta, item!)
                self.itemTextField.text = ""
            }
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "amigos" {
            let destino = segue.destination as? ConviteController
            destino?.festa = self.festa
        } else if segue.identifier == "popoverItem" {
            let destino = segue.destination as! PopoverItemController
            if let index = self.tableView.indexPathForSelectedRow {
                let selectedCellSourceView = tableView.cellForRow(at: index) as! CellItem
                destino.item = self.itens[index.row]
            }
        }
    }
}

extension ChecklistViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "popoverItem", sender: self)
    }
    
}

extension ChecklistViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath) as! CellItem
        
        cell.item = itens[indexPath.row]
        
        return cell
    }
}

extension ChecklistViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
