//
//  Convite.swift
//  Partify
//
//  Created by Thiago Vinhote on 21/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class Convite: NSObject {

    var id : String?
    var toId: String?
    var fromId: String?
    var idFesta: String?
    var data: NSNumber?
    var status: NSNumber?
    
    convenience init(dic: [String: Any]) {
        self.init()
        self.setValuesForKeys(dic)
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if self.responds(to: NSSelectorFromString(key)) {
            super.setValue(value, forKey: key)
        }
    }
    
    override var description: String {
        return "\(self.toId) \(self.fromId) \(self.idFesta)"
    }
}
