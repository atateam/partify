//
//  ConviteController.swift
//  Partify
//
//  Created by Thiago Vinhote on 21/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class ConviteController: UIViewController {

    var friends: [Friend] = []
    weak var festa: Festa?
    var addedFriends: [Friend] = []
    var isEditingParty: Bool = false
    var addedUsers: [User] = []
    @IBOutlet weak var tableFriends: UITableView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionAddedFriends: UICollectionView!
    @IBOutlet weak var continueButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.tableFriends.tableFooterView = UIView()
        self.perform(#selector(self.consultaDeAmigoFB), with: self, afterDelay: 0)
    }

    //função de teste, consulta de amigos no facebook
    internal func consultaDeAmigoFB() -> Void {
        self.lock()
        UserStore.singleton.graphRequestFacebookFriends { (friends: [Friend]?, error: Error?) in
            if let fs = friends {
                self.friends = fs
            }
            DispatchQueue.main.async {
                self.tableFriends.reloadData()
                self.unlock()
            }
        }
    }

    @IBAction func cancelPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func continuePressed(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "continueParty", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! CreatePartyViewController
        vc.addedFriends = self.addedFriends
    }
    
    func setup() {
        let valor: CGFloat
        if addedFriends.isEmpty && addedUsers.isEmpty {
            valor = 0
            continueButton.isEnabled = false
        } else {
            valor = 68
            continueButton.isEnabled = true
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.collectionHeight.constant = valor
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
}

extension ConviteController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isEditingParty {
            return self.addedUsers.count
        }
        return self.addedFriends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! CellFriend
        if isEditingParty {
            cell.user = self.addedUsers[indexPath.item]
        } else {
            cell.friend = self.addedFriends[indexPath.item]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isEditingParty {
            let f = self.addedUsers.remove(at: indexPath.item)
            self.setup()
            collectionView.deleteItems(at: [indexPath])
            if let index = self.addedUsers.index(of: f) {
                let cell = self.tableFriends.cellForRow(at: IndexPath(row: index, section: 0)) as! CellFriendTable
                cell.accessoryType = .none
            }
        } else {
            let f = self.addedFriends.remove(at: indexPath.item)
            self.setup()
            collectionView.deleteItems(at: [indexPath])
            if let index = self.friends.index(of: f) {
                let cell = self.tableFriends.cellForRow(at: IndexPath(row: index, section: 0)) as! CellFriendTable
                cell.accessoryType = .none
            }
        }
    }
}

extension ConviteController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! CellFriendTable
        cell.friend = self.friends[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let f =  friends[indexPath.row]
        if !self.addedFriends.contains(f) {
            self.addedFriends.append(f)
            let cell = tableView.cellForRow(at: indexPath) as! CellFriendTable
            cell.accessoryType = .checkmark
            if let index = self.addedFriends.index(of: f) {
                self.collectionAddedFriends.insertItems(at: [IndexPath(item: index, section: 0)])
            }
            self.setup()
        }
    }
}
