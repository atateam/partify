//
//  ConviteNotificationController.swift
//  Partify
//
//  Created by Thiago Vinhote on 22/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class ConviteNotificationController: UIViewController {

    var convites: [Convite] = []
    
    @IBOutlet weak var navbar: UINavigationBar!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var collectionConvite: UICollectionView!
    
    weak var partiesController: PartiesViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchConvites()
        self.fetchRemoveConvites()
        self.setup()
    }
    
    private func setup() {
        self.navbar.setBackgroundImage(UIImage(), for: .default)
        self.backgroundView.layer.shadowColor = UIColor.black.cgColor
        self.backgroundView.layer.shadowRadius = 10
        self.backgroundView.layer.bounds = self.backgroundView.bounds
        self.backgroundView.layer.shadowOpacity = 0.5
        self.backgroundView.layer.shadowOffset = CGSize.zero
    }

    @IBAction func handleFechar() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func fetchConvites(){
        ConviteStore.singleton.fetchConvites() { (convite: Convite?) in
            if let convite = convite {
                print(convite)
                self.convites.append(convite)
            }
            DispatchQueue.main.async {
                self.collectionConvite.reloadData()
            }
        }
    }
    
    private func fetchRemoveConvites(){
        ConviteStore.singleton.fetchRemoveConvites { (key: String) in
            let index = self.convites.index(where: { (c: Convite) -> Bool in
                return c.id == key
            })
            if let i = index {
                self.convites.remove(at: i)
                DispatchQueue.main.async {
                    self.collectionConvite.deleteItems(at: [IndexPath(item: i, section: 0)])
                }
            }

        }
    }

}

extension ConviteNotificationController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.convites.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! CellConvite
        cell.convite = self.convites[indexPath.item]
        return cell
    }
}

extension ConviteNotificationController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.backgroundView.frame.width - 20, height: 128)
    }
    
}
