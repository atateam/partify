//
//  ConviteStore.swift
//  Partify
//
//  Created by Thiago Vinhote on 21/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import Foundation
import Firebase

class ConviteStore: NSObject {

    static let singleton: ConviteStore = ConviteStore()
    
    let conviteReference: FIRDatabaseReference = {
        return FIRDatabase.database().reference().child("convites")
    }()
    
    private override init() {
        super.init()
    }
    
//    func removeFestaItem(festaId id: String, _ item: Item, completion: @escaping (_ item: Item?, _ error: Error?) -> Void) -> Void {
//        guard let idItem = item.id else {
//            return
//        }
//        let refFestaItens = FIRDatabase.database().reference().child("festa-itens").child(id).child(idItem)
//        refFestaItens.removeValue { (error: Error?, ref: FIRDatabaseReference) in
//            if let e = error {
//                print(e)
//                completion(nil, e)
//                return
//            }
//            DispatchQueue.main.async {
//                completion(item, nil)
//            }
//        }
//    }
    
//    func removeItem(_ item: Item, completion: @escaping (_ item: Item?, _ error: Error?) -> Void) -> Void {
//        guard let id = item.id else {
//            return
//        }
//        let childRef = itemReference.child(id)
//        childRef.removeValue { (error: Error?, ref: FIRDatabaseReference) in
//            if let e = error {
//                print(e)
//                completion(nil, e)
//                return
//            }
//            DispatchQueue.main.async {
//                completion(item, nil)
//            }
//        }
//    }
    
    func removeUserConvite(_ convite: Convite, completion: @escaping (_ convite: Convite?, _ error: Error?) -> Void) -> Void{
        guard let id = convite.id, let idUser = UserStore.singleton.user?.id else {
            return
        }
        let childRef = FIRDatabase.database().reference().child("user-convites").child(idUser).child(id)
        childRef.removeValue { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, error)
                return
            }
            completion(convite, nil)
        }
    }
    
    func fetchRemoveConvites(completion: @escaping (_ key: String) -> Void) -> Void {
        guard let id = UserStore.singleton.user?.id else {
            return
        }
        let refUserConvites = FIRDatabase.database().reference().child("user-convites").child(id)
        refUserConvites.observe(.childRemoved, with: { (snapshot: FIRDataSnapshot) in
            completion(snapshot.key)
        }, withCancel: nil)
    }
    
    func fetchConvites(completion: @escaping (_ convite: Convite?) -> Void) -> Void {
        guard let id = UserStore.singleton.user?.id else {
            return
        }
        let refUserConvites = FIRDatabase.database().reference().child("user-convites").child(id)
        refUserConvites.removeAllObservers()
        refUserConvites.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String: Any] {
                let convite = Convite(dic: dic)
                convite.id = snapshot.key
                completion(convite)
            } else {
                completion(nil)
            }
        }, withCancel: nil)
    }
    
    func fetchConvitesWatch(completion: @escaping (_ convite: [Convite]) -> Void) -> Void {
        guard let id = UserStore.singleton.user?.id else {
            return
        }
        var convites: [Convite] = []
        let group = DispatchGroup()
        let refUserConvites = FIRDatabase.database().reference().child("user-convites").child(id)
        group.enter()
        refUserConvites.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            group.enter()
            if let dic = snapshot.value as? [String: Any] {
                let convite = Convite(dic: dic)
                convite.id = snapshot.key
                convites.append(convite)
            }
            group.leave()
            
        }, withCancel: nil)
        group.leave()
        DispatchQueue(label: "conviteWatch").asyncAfter(deadline: DispatchTime.now() + .seconds(1), execute: {
            group.notify(queue: DispatchQueue.main, execute: {
                completion(convites)
            })
        })
    }
    
    func createUserConvites(_ convite: Convite, completion: @escaping (_ convite: Convite?, _ error: Error?) -> Void) -> Void {
        guard let fromId = UserStore.singleton.user?.id, let toId = convite.toId, let idFesta = convite.idFesta, let data = convite.data, let status = convite.status else {
            completion(nil, nil)
            return
        }
        let values: [String: Any] = ["fromId": fromId, "toId": toId, "idFesta": idFesta, "data": data, "status": status]
        let recipientUserConviteRef = FIRDatabase.database().reference().child("user-convites").child(toId).childByAutoId()
        recipientUserConviteRef.updateChildValues(values, withCompletionBlock: { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, error)
                return
            }
            convite.id = ref.key
            convite.fromId = fromId
            completion(convite, nil)
        })
    }
    
    func confirmaConvite(_ convite: Convite, completion: @escaping (_ convite: Convite?, _ error: Error?) -> Void) -> Void {
        guard let toId = convite.toId, let idFesta = convite.idFesta else {
            completion(nil, nil)
            return
        }
        
        FestaStore.singleton.addUserFesta(withId: toId, idFesta: idFesta, completion: { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, e)
                return
            }
            ConviteStore.singleton.removeUserConvite(convite, completion: { (convite: Convite?, error: Error?) in
                completion(convite, error)
            })
        })
    }
    
    func recusarConvite(_ convite: Convite, completion: @escaping (_ convite: Convite?, _ error: Error?) -> Void) -> Void {
        ConviteStore.singleton.removeUserConvite(convite, completion: { (convite: Convite?, error: Error?) in
            completion(convite, error)
        })
    }
    
}
