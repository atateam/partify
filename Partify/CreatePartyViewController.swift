//
//  CreatePartyViewController.swift
//  Partify
//
//  Created by Arthur Melo on 10/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import UserNotifications

class CreatePartyViewController: UIViewController {
    
    var isEditingParty: Bool = false
    var festa: Festa = Festa()
    var users: [User] = []
    
    @IBOutlet weak var partyName: UITextField! {
        didSet {
            partyName.attributedPlaceholder = NSAttributedString(string: "Party name", attributes: [NSForegroundColorAttributeName: UIColor.prtBlueberry])
        }
    }
    @IBOutlet weak var partyDescription: UITextView! {
        didSet {
            partyDescription.layer.cornerRadius = 5
            partyDescription.layer.borderWidth = 1.0
            partyDescription.layer.borderColor = UIColor.prtBlueberry.cgColor
        }
        
    }
    @IBOutlet weak var partyDate: UITextField! {
        didSet{
            partyDate.attributedPlaceholder = NSAttributedString(string: "Date", attributes: [NSForegroundColorAttributeName: UIColor.prtBlueberry])

            let toolbar = UIToolbar(frame: CGRect(x: 0, y: UIScreen.main.bounds.height / 6, width: UIScreen.main.bounds.width, height: 40.0))
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.donePressed))
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
            toolbar.setItems([flexSpace, doneButton], animated: true)
            partyDate.inputAccessoryView = toolbar
            partyDate.inputView = UIDatePicker()
        }
    }
    @IBOutlet weak var partyProfileImage: UIImageView! {
        didSet {
            partyProfileImage.layer.cornerRadius = partyProfileImage.frame.width / 2
            partyProfileImage.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var partyEnd: UITextField! {
        didSet {
            partyEnd.attributedPlaceholder = NSAttributedString(string: "Location", attributes: [NSForegroundColorAttributeName: UIColor.prtBlueberry])
        }
    }
    
    @IBOutlet weak var confirmButton: UIButton! {
        didSet {
            confirmButton.layer.cornerRadius = confirmButton.frame.height / 2
        }
    }
    var addedFriends: [Friend] = []
    
    @IBOutlet weak var collectionFriends: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        partyName.delegate = self
        partyDate.delegate = self
        partyEnd.delegate = self
        partyDescription.delegate = self
        
        self.partyProfileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleUploadTap)))
        print(addedFriends)
        
        if isEditingParty {
            self.partyProfileImage.loadImageUsingCache(withUrlString: self.festa.imageProfileUrl!)
            self.partyName.text = self.festa.nome
            if let data = self.festa.data {
                let date = Date(timeIntervalSince1970: TimeInterval(data))
                let formate = DateFormatter()
                formate.dateFormat = "dd-MM-yyyy, HH:mm"
                partyDate.text = formate.string(from: date)
            }
            self.partyEnd.text = self.festa.local
            self.partyDescription.text = self.festa.descricao!
            collectionFriends.reloadData()
        }
    }
    
    override func viewDidLayoutSubviews() {
        partyName.addBottomLayer(height: 1.0, color: UIColor.prtBlueberry)
        partyDate.addBottomLayer(height: 1.0, color: UIColor.prtBlueberry)
        partyEnd.addBottomLayer(height: 1.0, color: UIColor.prtBlueberry)
    }
    
    func donePressed (_ sender: UIBarButtonItem) {
        partyDate.resignFirstResponder()
        let dateFormat = DateFormatter()
        dateFormat.dateStyle = .medium
        dateFormat.timeStyle = .short
        if let input = partyDate.inputView as? UIDatePicker {
            partyDate.text = dateFormat.string(from: input.date)
        }
    }
    
    @IBAction func savePressed(_ sender: UIButton) {
        let festa = Festa()
        festa.nome = self.partyName.text
        festa.descricao = self.partyDescription.text
        if let data = (self.partyDate.inputView as? UIDatePicker)?.date {
            festa.data = Int(data.timeIntervalSince1970) as NSNumber
        }
        festa.local = self.partyEnd.text
        
        if let imageProfile = self.partyProfileImage.image {
            self.lock()
            StoreImage.singleton.uploadToFirebaseStorage(using: imageProfile, completion: { (imageUrl: String, error: Error?) in
                festa.imageProfileUrl = imageUrl
                FestaStore.singleton.createFesta(festa) { (festa: Festa?, error: Error?) in
                    self.unlock()
                    if error != nil {
                        print("Aconteceu algo de errado ao tentar salvar!", error!)
                        return
                    }
                    if let id = UserStore.singleton.user?.id, let idFesta = festa?.id {
                        FestaStore.singleton.addUserFesta(withId: id, idFesta: idFesta, completion: nil)
                    }
                    if let f = festa {
                        FestaStore.singleton.createUserFesta(f)
                        for i in 0...self.addedFriends.count - 1 {
                            let convite = Convite()
                            convite.data = Int(Date().timeIntervalSince1970) as NSNumber?
                            convite.idFesta = f.id
                            convite.toId = self.addedFriends[i].id
                            convite.status = false
                            ConviteStore.singleton.createUserConvites(convite, completion: { (convite: Convite?, error: Error?) in
                                if let e = error {
                                    print(e)
                                    return
                                }
                            })
                        }

                    }
                    self.scheduleNotifications(festa!)
                    self.dismiss(animated: true, completion: nil)
                }
            })
        } else {
            print(#function, "Você não escolheu uma imagem")
        }
        
        // Passando por todos os amigos em addedFriends e enviando-lhes o convite
    }
    
    func scheduleNotifications(_ festa: Festa) {
        let content = UNMutableNotificationContent()
        
        content.title = festa.nome!
        content.body = "Está chegando a festa!"
        content.sound = UNNotificationSound.default()
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
        let request = UNNotificationRequest.init(identifier: "FiveSecond", content: content, trigger: trigger)
        
        let aceitar = UNNotificationAction.init(identifier: "Aceitar", title: "Aceitar", options: .authenticationRequired)
        let recusar = UNNotificationAction.init(identifier: "Recusar", title: "Recusar", options: .destructive)
        
        let category = UNNotificationCategory(identifier: "category", actions: [aceitar, recusar], intentIdentifiers: [], options: .allowInCarPlay)
        
        let center = UNUserNotificationCenter.current()
        center.setNotificationCategories([category])
        center.add(request) { (error) in
            print(error)
        }
    }

    func handleUploadTap(tap: UITapGestureRecognizer) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.navigationBar.tintColor = .white
        imagePickerController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.present(imagePickerController, animated: true, completion: nil)
    }
}

extension CreatePartyViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isEditingParty {
            return self.users.count + 1
        }
        return self.addedFriends.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! CellFriend
        if indexPath.item == 0 {
            cell.imageProfile.image = #imageLiteral(resourceName: "profile")
            return cell
        }
        
        if isEditingParty {
            cell.user = self.users[indexPath.item - 1]
        } else {
            cell.friend = self.addedFriends[indexPath.item - 1]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            if isEditingParty {
                let storyboard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "convites") as! ConviteController
                storyboard.isEditingParty = isEditingParty
                storyboard.addedUsers = users
                self.navigationController?.pushViewController(storyboard, animated: true)
            } else {
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension CreatePartyViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.navigationBar.barStyle = .black
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImage = originalImage
        }
        self.partyProfileImage.image = selectedImage
        picker.navigationBar.barStyle = .black
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension CreatePartyViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

extension CreatePartyViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
