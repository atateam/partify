//
//  Enquete.swift
//  Partify
//
//  Created by Arthur Melo on 16/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class Enquete: NSObject {
    
    var id: String?
    var nome: String?
    var pergunta: String?
    var data: NSNumber?
    
    convenience init(dic: [String: Any]) {
        self.init()
        self.setValuesForKeys(dic)
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if self.responds(to: NSSelectorFromString(key)){
            super.setValue(value, forKey: key)
        }
    }
    
    override var description: String {
        return "\(nome), \(data)"
    }
}
