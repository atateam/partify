//
//  EnqueteStore.swift
//  Partify
//
//  Created by Arthur Melo on 16/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import Firebase

class EnqueteStore: NSObject {
    
    static let singleton: EnqueteStore = EnqueteStore()
    
    let enquetesReference : FIRDatabaseReference = {
        return FIRDatabase.database().reference().child("enquetes")
    }()
    
    private override init() {
        super.init()
    }
    
    func createEnquete(_ enquete: Enquete, completion: @escaping (_ enquete: Enquete?, _ error: Error?) -> Void) {
        guard let nome = enquete.nome, let pergunta = enquete.pergunta, let data = enquete.data else {
            return
        }
        let values: [String: Any] = ["nome": nome, "pergunta": pergunta, "data": data]
        let childRef = enquetesReference.childByAutoId()
        childRef.updateChildValues(values) { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, error)
                return
            }
            enquete.id = childRef.key
            DispatchQueue.main.async {
                completion(enquete, nil)
            }
        }
    }
    
    func removeEnquete(_ enquete: Enquete, completion: @escaping (_ enquete: Enquete?, _ error: Error?) -> Void) {
        guard let id = enquete.id else {
            return
        }
        let childRef = enquetesReference.child(id)
        childRef.removeValue { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, error)
                return
            }
            DispatchQueue.main.async {
                completion(enquete, nil)
            }
        }
    }
    
    func updateEnquete(_ enquete: Enquete, completion: @escaping (_ enquete: Enquete?, _ error: Error?) -> Void) {
        guard let id = enquete.id else {
            return
        }
        
        guard let nome = enquete.nome, let pergunta = enquete.pergunta, let data = enquete.data else {
            return
        }
        
        let values: [String: Any] = ["nome": nome, "pergunta": pergunta, "data": data]
        let childRef = enquetesReference.child(id)
        childRef.updateChildValues(values) { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, error)
                return
            }
            completion(enquete, nil)
        }
    }
    
    func fetchEnquete(withId id: String, completion: @escaping (_ enquete: Enquete?) -> Void) -> Void {
        let childRef = enquetesReference.child(id)
        childRef.observeSingleEvent(of: .value, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String: Any] {
                let enquete = Enquete(dic: dic)
                enquete.id = snapshot.key
                completion(enquete)
            } else {
                completion(nil)
            }
        }, withCancel: nil)
    }
    
    func fetchEnquetes(idFesta id: String, completion: @escaping (_ key: String) -> Void) -> Void {
        let refFestaEnquetes = FIRDatabase.database().reference().child("festa-enquetes").child(id)
        refFestaEnquetes.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            completion(snapshot.key)
        }, withCancel: nil)
    }
    
    func createFestaEnquete(idFesta id: String, _ enquete: Enquete) {
        let refFestaEnquete = FIRDatabase.database().reference().child("festa-enquetes").child(id)
        if let idEnquete = enquete.id {
            refFestaEnquete.updateChildValues([idEnquete: 1])
        }
    }
}
