//
//  Extension+String.swift
//  Partify
//
//  Created by Thiago Vinhote on 25/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

extension String {
    
    func rect(view: UIView, sizeFont: CGFloat? = 14) -> CGRect {
        let rect = NSString(string: self).boundingRect(with: CGSize(width: view.frame.width - 20, height: 1000), options: NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin), attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: sizeFont!)], context: nil)
        return rect
    }
    
}
