//
//  Extension+UIButton.swift
//  Partify
//
//  Created by Arthur Melo on 24/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

extension UITextField {
    func addBottomLayer(height: CGFloat, color: UIColor) {
        let border = CALayer()
        border.borderColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - height, width: self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = height
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
