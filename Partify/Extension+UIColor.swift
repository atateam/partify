//
//  Extension+UIColor.swift
//  Partify
//
//  Created by Thiago Vinhote on 24/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

extension UIColor {
    class var prtLavenderPink: UIColor {
        return UIColor(red: 201.0 / 255.0, green: 109.0 / 255.0, blue: 216.0 / 255.0, alpha: 1.0)
    }
    
    class var prtSapphire: UIColor {
        return UIColor(red: 48.0 / 255.0, green: 35.0 / 255.0, blue: 174.0 / 255.0, alpha: 1.0)
    }
    
    class var prtWhite30: UIColor {
        return UIColor(white: 255.0 / 255.0, alpha: 0.3)
    }
    
    class var prtLightIndigo: UIColor {
        return UIColor(red: 133.0 / 255.0, green: 102.0 / 255.0, blue: 202.0 / 255.0, alpha: 1.0)
    }
    
    class var prtBlueberry: UIColor {
        return UIColor(red: 98.0 / 255.0, green: 69.0 / 255.0, blue: 161.0 / 255.0, alpha: 1.0)
    }
    
    class var prtPalePurple: UIColor {
        return UIColor(red: 168.0 / 255.0, green: 129.0 / 255.0, blue: 213.0 / 255.0, alpha: 1.0)
    }
    
    class var prtLightIndigoTwo: UIColor {
        return UIColor(red: 128.0 / 255.0, green: 96.0 / 255.0, blue: 202.0 / 255.0, alpha: 1.0)
    }
    
    class var prtAmethyst: UIColor {
        return UIColor(red: 151.0 / 255.0, green: 116.0 / 255.0, blue: 207.0 / 255.0, alpha: 1.0)
    }
}
