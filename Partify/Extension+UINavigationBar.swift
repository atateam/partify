//
//  Extension+UINavigationBar.swift
//  Partify
//
//  Created by Arthur Melo on 23/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import QuartzCore

extension CAGradientLayer {
    class func gradientLayerForBounds(bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [UIColor.red, UIColor.blue]
        return layer
    }
}
