//
//  Extension+UIView.swift
//  Partify
//
//  Created by Thiago Vinhote on 25/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

extension UIView {
    
    func setGradient(colors: [UIColor]) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors.map({return $0.cgColor})
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func addMaskWhite(white: CGFloat? = 1, alpha: CGFloat? = 0.3) {
        let layer = CALayer()
        layer.backgroundColor = UIColor(white: white!, alpha: alpha!).cgColor
        layer.bounds = self.bounds
        layer.anchorPoint = CGPoint(x: 0, y: 0)
        self.layer.addSublayer(layer)
    }
 
    func setShadow(withRadius radius: CGFloat? = 5, opacity: Float? = 0.3) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = radius!
        self.layer.bounds = self.bounds
        self.layer.shadowOpacity = opacity!
        self.layer.shadowOffset = CGSize.zero
        self.layer.masksToBounds = false
    }
    
}
