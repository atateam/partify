//
//  Extension+UIViewControllerDismissOnTap.swift
//  Partify
//
//  Created by Arthur Melo on 25/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let swipe: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        self.view.addGestureRecognizer(swipe)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
