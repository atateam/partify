//
//  Extension+UIimage.swift
//  Partify
//
//  Created by Thiago Vinhote on 29/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

extension UIImage {
    class func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage!
    }
}
