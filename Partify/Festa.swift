//
//  Festa.swift
//  Partify
//
//  Created by Thiago Vinhote on 10/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class Festa: NSObject {
    
    var id: String?
    var nome: String?
    var descricao: String?
    var data: NSNumber?
    var local: String?
    var imageProfileUrl: String?
    
    convenience init(dic: [String: Any]) {
        self.init()
        self.setValuesForKeys(dic)
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if self.responds(to: NSSelectorFromString(key)){
            super.setValue(value, forKey: key)
        }
    }
    
    override var description: String {
        return "\(nome), \(data)"
    }
}
