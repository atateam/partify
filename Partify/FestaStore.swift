//
//  FestaStore.swift
//  Partify
//
//  Created by Thiago Vinhote on 10/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import Firebase

class FestaStore: NSObject {
    
    static let singleton: FestaStore = FestaStore()
    
    let festasReference : FIRDatabaseReference = {
        return FIRDatabase.database().reference().child("festas")
    }()
    
    private override init() {
        super.init()
    }
    
    /// Cria uma nova festa no database do Firebase.
    ///
    /// Quando algum do atributos da classes 'Festa' estar 'nil' é executado o completion
    /// retornano os parâmentros 'festa' e 'error' nulos.
    ///
    /// Em caso de sucesso é chamado o completion passando no parâmetro 'festa'
    /// o objeto com os valores criados no Firebase e o 'error' ficará nil.
    /// 
    /// Em situação de falha na persistência dos dados no Firebase, os valores
    /// dos parâmetros serão:
    ///
    /// - festa: esse atributo ficará nil
    /// - error: será o error causador da falha. Um print do error será mostrado no log.
    ///
    ///
    ///     FestaStore.singleton.createFesta(festa) { (festa: Festa?, error: Error?) in
    ///         if error != nil {
    ///             print("Aconteceu algo de errado ao tentar salvar!", error!)
    ///             return
    ///         }
    ///         print(festa)
    ///     }
    ///     // Print "Aconteceu algo de errado ao tentar salvar! Erro causando da falha"
    ///
    ///
    ///     FestaStore.singleton.createFesta(festa) { (festa: Festa?, error: Error?) in
    ///         if error != nil {
    ///             print(error!)
    ///             return
    ///         }
    ///         print("Festa salva com sucesso!", festa)
    ///     }
    ///     // Print "Festa salva com sucesso Optional(nome, data)"
    ///
    /// - Parameters:
    ///   - festa: Objeto contendo os valores que serão persistidos
    ///   - completion: O quer será executudo quando ocorrer algum resultado
    ///   - error: Objeto contendo a falha caso ela ocorra
    /// - Returns: Retorno da função void e completion escapando após o retorno da função
    func createFesta(_ festa: Festa, completion: @escaping (_ festa: Festa?, _ error: Error?) -> Void) {
        guard let nome = festa.nome, let descricao = festa.descricao, let data = festa.data, let imageUrl = festa.imageProfileUrl, let local = festa.local else {
            completion(nil, nil)
            return
        }
        let values: [String: Any] = ["nome": nome, "descricao": descricao, "data": data, "imageProfileUrl": imageUrl, "local": local]
        let childRef = festasReference.childByAutoId() 
        childRef.updateChildValues(values) { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, error)
                return
            }
            festa.id = ref.key
            completion(festa, nil)
        }
    }
    
    func removeFesta(_ festa: Festa, completion: @escaping (_ festa: Festa?, _ error: Error?) -> Void) {
        guard let id = festa.id else {
            return
        }
        let childRef = festasReference.child(id)
        childRef.removeValue { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, error)
                return
            }
            completion(festa, nil)
        }
    }
    
    func removeUserFesta(_ festa: Festa, completion: @escaping (_ convite: Festa?, _ error: Error?) -> Void) -> Void{
        guard let id = festa.id, let idUser = UserStore.singleton.user?.id else {
            return
        }
        let childRef = FIRDatabase.database().reference().child("user-festas").child(idUser).child(id)
        childRef.removeValue { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, error)
                return
            
            }
            let chilFestaUser = FIRDatabase.database().reference().child("festa-users").child(id).child(idUser)
            chilFestaUser.removeValue()
            completion(festa, nil)
        }
    }
    
    func updateFesta(_ festa: Festa, completion: @escaping (_ festa: Festa?, _ error: Error?) -> Void) {
        guard let id = festa.id else {
            return
        }
        
        guard let nome = festa.nome, let descricao = festa.descricao, let data = festa.data, let imageUrl = festa.imageProfileUrl, let local = festa.local else {
            return
        }
        
        let values: [String: Any] = ["nome": nome, "descricao": descricao, "data": data, "imageProfileUrl": imageUrl, "local": local]
        
        let childRef = festasReference.child(id)
        childRef.updateChildValues(values) { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, error)
                return
            }
            completion(festa, nil)
        }
    }
    
    func fetchRemoveFesta(completion: @escaping (_ key: String) -> Void) -> Void{
        guard let uid = UserStore.singleton.user?.id else {
            completion("")
            return
        }
        let refUserFestas = FIRDatabase.database().reference().child("user-festas").child(uid)
        refUserFestas.observe(.childRemoved, with: { (snapshot: FIRDataSnapshot) in
            completion(snapshot.key)
        }, withCancel: nil)
    }
    
    func fetchFesta(withId id: String, completion: @escaping (_ festa: Festa?) -> Void) -> Void {
        let childRef = festasReference.child(id)
        childRef.observeSingleEvent(of: .value, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String: Any] {
                let festa = Festa(dic: dic)
                festa.id = snapshot.key
                completion(festa)
            } else {
                completion(nil)
            }
        }, withCancel: nil)
    }
    
    func fetchFestaUsers(withId idFesta: String, completion: @escaping (_ key: String) -> Void) -> () {
        let ref = FIRDatabase.database().reference().child("festa-users").child(idFesta)
        ref.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            completion(snapshot.key)
        }, withCancel: nil)
    }
    
    func addUserFesta(withId id: String, idFesta: String, completion: ((_ error: Error?, _ reference: FIRDatabaseReference) -> Void)?) -> Void {
        let refUserFesta = FIRDatabase.database().reference().child("user-festas").child(id)
        refUserFesta.updateChildValues([idFesta: 1], withCompletionBlock: { (error: Error?, ref: FIRDatabaseReference) in
            let refFestaUser = FIRDatabase.database().reference().child("festa-users").child(idFesta)
            refFestaUser.updateChildValues([id: 1])
            completion?(error, ref)
        })
    }
    
    func fetchFestas(completion: @escaping (_ key: String) -> Void) -> Void {
//        self.festasReference.removeAllObservers()
        guard let uid = UserStore.singleton.user?.id else {
            completion("")
            return
        }
        let refUserFestas = FIRDatabase.database().reference().child("user-festas").child(uid)
        refUserFestas.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            completion(snapshot.key)
        }, withCancel: { (error: Error) in
            print(error)
        })
    }
    
    func fetchFestasWatch(completion: @escaping (_ festas: [Festa]) -> Void) {
        guard let uid = UserStore.singleton.user?.id else {
            return
        }
        var festas: [Festa] = []
        let refUserFestas = FIRDatabase.database().reference().child("user-festas").child(uid)
        let group = DispatchGroup()
        
        refUserFestas.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            group.enter()
            
            self.fetchFesta(withId: snapshot.key, completion: { (festa: Festa?) in
                if let f = festa {
                    festas.append(f)
                }
                group.leave()
            })
            
        }, withCancel: nil)
        
        let delay = DispatchTime.now() + .seconds(1)
        DispatchQueue(label: "watch").asyncAfter(deadline: delay, execute: {
            group.notify(queue: DispatchQueue.main, execute: {
                completion(festas)
            })
        })
    }
    
    func createUserFesta(_ festa: Festa) {
        guard let uid = UserStore.singleton.user?.id else {
            return
        }
        let refUserFesta = FIRDatabase.database().reference().child("user-festas").child(uid)
        if let idFesta = festa.id {
            refUserFesta.updateChildValues([idFesta: 1])
        }
    }
}
