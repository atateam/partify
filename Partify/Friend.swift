//
//  Friend.swift
//  Partify
//
//  Created by Thiago Vinhote on 21/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class Friend: NSObject {

    var id: String?
    var name: String?
    var urlImageProfile: String?
    
    convenience init(dic: [String: Any]) {
        self.init()
        self.setValuesForKeys(dic)
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if self.responds(to: NSSelectorFromString(key)) {
            super.setValue(value, forKey: key)
        } else if key == "picture" {
            if let picture = value as? [String: Any], let data = picture["data"] as? [String: Any],  let url = data["url"] as? String {
                super.setValue(url, forKey: "urlImageProfile")
            }
        }
    }
    
    override var description: String{
        return "\(self.name) \(self.id)"
    }
}
