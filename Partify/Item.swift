//
//  Item.swift
//  Partify
//
//  Created by Thiago Vinhote on 15/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class Item: NSObject {

    var id: String?
    var nome: String?
    
    var observerFetch: UInt?
    var observerFetchRemove: UInt?
    
    convenience init(dic: [String: Any]) {
        self.init()
        self.setValuesForKeys(dic)
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if self.responds(to: NSSelectorFromString(key)) {
            super.setValue(value, forKey: key)
        }
    }
}
