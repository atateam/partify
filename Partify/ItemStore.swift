//
//  ItemStore.swift
//  Partify
//
//  Created by Thiago Vinhote on 15/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import Firebase

class ItemStore: NSObject {
    
    static let singleton: ItemStore = ItemStore()
    
    let itemReference: FIRDatabaseReference = {
        return FIRDatabase.database().reference().child("itens")
    }()
    
    private override init() {
        super.init()
    }
    
    func createItem(_ item: Item, completion: @escaping (_ item: Item?, _ error: Error?) -> Void) -> Void {
        guard let nome = item.nome else {
            return
        }
        let values: [String: Any] = ["nome": nome]
        let childRef = itemReference.childByAutoId()
        childRef.updateChildValues(values) { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, error)
                return
            }
            item.id = ref.key
            DispatchQueue.main.async {
                completion(item, nil)
            }
        }
    }
    
    func removeFestaItem(festaId id: String, _ item: Item, completion: @escaping (_ item: Item?, _ error: Error?) -> Void) -> Void {
        guard let idItem = item.id else {
            return
        }
        let refFestaItens = FIRDatabase.database().reference().child("festa-itens").child(id).child(idItem)
        refFestaItens.removeValue { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, e)
                return
            }
            DispatchQueue.main.async {
                completion(item, nil)
            }
        }
    }
    
    func removeItem(_ item: Item, completion: @escaping (_ item: Item?, _ error: Error?) -> Void) -> Void {
        guard let id = item.id else {
            return
        }
        let childRef = itemReference.child(id)
        childRef.removeValue { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion(nil, e)
                return
            }
            DispatchQueue.main.async {
                completion(item, nil)
            }
        }
    }
    
    func fetchItem(withId id: String, completion: @escaping (_ item: Item?) -> Void) -> Void {
        let childRef = itemReference.child(id)
        childRef.observeSingleEvent(of: .value, with: { (snapshot: FIRDataSnapshot) in
            if let dic = snapshot.value as? [String: Any] {
                let item = Item(dic: dic)
                item.id = snapshot.key
                completion(item)
            } else {
                completion(nil)
            }
        }, withCancel: nil)
    }
    
    func fetchItens(festaId id: String, completion: @escaping (_ key: String) -> Void) -> Void {
        let refUserFestas = FIRDatabase.database().reference().child("festa-itens").child(id)
        refUserFestas.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            completion(snapshot.key)
        }, withCancel: nil)
    }
    
    func fetchItemWatch(withId idFesta: String, completion: @escaping (_ itens: [String]) -> Void) -> () {
        var itens = [String]()
        let group = DispatchGroup()
        let refUserFestas = FIRDatabase.database().reference().child("festa-itens").child(idFesta)
        refUserFestas.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) in
            group.enter()
            self.itemReference.child(snapshot.key).observeSingleEvent(of: .value, with: { (snapshot: FIRDataSnapshot) in
                if let dic = snapshot.value as? [String: Any] {
                    itens.append(Item(dic: dic).nome!)
                }
                group.leave()
            }, withCancel: nil)
        }, withCancel: nil)
        let delay = DispatchTime.now() + .seconds(1)
        DispatchQueue(label: "watchItens").asyncAfter(deadline: delay, execute: {
            group.notify(queue: DispatchQueue.main, execute: {
                completion(itens)
            })
        })
    }
    
    func createFestaItem(festaId id: String, _ item: Item) -> Void {
        let refFestaItens = FIRDatabase.database().reference().child("festa-itens").child(id)
        if let idItem = item.id {
            refFestaItens.updateChildValues([idItem: 1])
        }
    }
    
    func addItemUser(withId idItem: String, idUser: String, completion: @escaping (_ error: Error?, _ ref: FIRDatabaseReference) -> Void) -> (){
        let refItemUser = FIRDatabase.database().reference().child("item-users").child(idItem)
        refItemUser.updateChildValues([idUser: 1]) { (error: Error?, ref: FIRDatabaseReference) -> Void in
            if let e = error {
                print(e)
                completion(e, ref)
                return
            }
            completion(nil, ref)
        }
    }
    
    func removeItemUser(withId idItem: String, idUser: String, completion: @escaping (_ error: Error?, _ ref: FIRDatabaseReference) -> Void) -> () {
        let refItemUser = FIRDatabase.database().reference().child("item-users").child(idItem).child(idUser)
        refItemUser.removeValue(completionBlock: completion)
    }
    
    func fetchItemUser(withId idItem: String, _ handleObserver: UInt?, completion: @escaping (_ user: User?, _ error: Error?) -> Void) -> UInt{
        let refItemUser = FIRDatabase.database().reference().child("item-users").child(idItem)
        if let handle = handleObserver {
            refItemUser.removeObserver(withHandle: handle)
        }
        return refItemUser.observe(.childAdded, with: { (snapshot: FIRDataSnapshot) -> Void in
            UserStore.singleton.fetchUser(withId: snapshot.key, completion: { (user: User?) in
                completion(user, nil)
            })
        }, withCancel: { (error: Error) -> Void in
            completion(nil, error)
        })
    }
    
    func fetchChangeddItemUser(withId idItem: String, completion: @escaping (_ user: User?, _ error: Error?) -> Void) -> (){
        let refItemUser = FIRDatabase.database().reference().child("item-users").child(idItem)
        refItemUser.observe(.childChanged, with: { (snapshot: FIRDataSnapshot) -> Void in
            UserStore.singleton.fetchUser(withId: snapshot.key, completion: { (user: User?) in
                completion(user, nil)
            })
        }, withCancel: { (error: Error) -> Void in
            completion(nil, error)
        })
    }
    
    func fetchRemovedItemUser(withId idItem: String, _ handleObserver: UInt?, completion: @escaping (_ user: User?, _ error: Error?) -> Void) -> UInt{
        let refItemUser = FIRDatabase.database().reference().child("item-users").child(idItem)
        if let handle = handleObserver {
            refItemUser.removeObserver(withHandle: handle)
        }
        return refItemUser.observe(.childRemoved, with: { (snapshot: FIRDataSnapshot) -> Void in
            UserStore.singleton.fetchUser(withId: snapshot.key, completion: { (user: User?) in
                completion(user, nil)
            })
        }, withCancel: { (error: Error) -> Void in
            completion(nil, error)
        })
    }
}
