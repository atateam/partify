//
//  ViewController.swift
//  Partify
//
//  Created by Thiago Vinhote on 10/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FirebaseInstanceID
import FirebaseMessaging

class LoginViewController: UIViewController {
    
    @IBOutlet weak var buttonLoginFacebook: FBSDKLoginButton!
    weak var partiesViewController: PartiesViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonLoginFacebook.delegate = self
        buttonLoginFacebook.readPermissions = ["public_profile", "email", "user_friends"]
    }

    internal func userLogoutFacebook() {
        UserStore.singleton.userLogout()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
    }
}

extension LoginViewController: FBSDKLoginButtonDelegate {
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        UserStore.singleton.userLogout()
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if let e = error {
            print(e)
            return
        }
        
        if result.isCancelled {
            return
        }
        self.lock()
        UserStore.singleton.singIn(withToken: FBSDKAccessToken.current().tokenString) { (user: User?, error: Error?) in
            UserStore.singleton.graphRequestFacebook(completion: { (result: Any?, error: Error?) in
                if let e = error {
                    print(e)
                    self.unlock()
                    return
                }
                
                guard let usuario = user else {
                    self.unlock()
                    return
                }
                
                if let dic = result as? NSDictionary {
                    if let firstName = dic.object(forKey: "first_name") {
                        if let lastName = dic.object(forKey: "last_name") {
                            usuario.nome = "\(firstName) \(lastName)"
                        } else {
                            usuario.nome = "\(firstName)"
                        }
                    }
                    if let email = dic.object(forKey: "email") as? String {
                        usuario.email = email
                    }
                    if let picture = dic.object(forKey: "picture") as? NSDictionary{
                        if let url = (picture.object(forKey: "data") as? NSDictionary)?.object(forKey: "url") as? String {
                            usuario.imageProfileUrl = url
                        }
                    }
                    if let refreshedToken = FIRInstanceID.instanceID().token() {
                        print("InstanceID token: \(refreshedToken)")
                        usuario.tokenMessaging = refreshedToken
                    }
                }
                
                UserStore.singleton.createUser(usuario, completion: { (user: User?, error: Error?) in
                    if error != nil {
                        self.unlock()
                        return
                    }
                    DispatchQueue.main.async {
                        self.unlock()
                        self.partiesViewController?.setupProfile(with: usuario)
                        self.dismiss(animated: true, completion: nil)
                        print("Usuário logado e criado no Database")
                    }
                })
            })
        }
    }
    
}
