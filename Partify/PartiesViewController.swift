//
//  PartiesViewController.swift
//  Partify
//
//  Created by Arthur Melo on 10/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import FirebaseInstanceID
import Firebase
import FirebaseMessaging
import UserNotifications

class PartiesViewController: UIViewController {
    
    @IBOutlet weak var collectionFesta: UICollectionView!
    @IBOutlet weak var buttonNotification: UIButton!
    
    @IBOutlet weak var profileImage: UIImageView! {
        didSet {
            profileImage.layer.cornerRadius = profileImage.frame.width / 2
            profileImage.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var usernameLabel: UILabel!
    
    var festas: [Festa] = []
    var enquetes: [Enquete] = []
    
    var selectedParty: Festa = Festa()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isUserLogged()
        
        collectionFesta.delegate = self
        collectionFesta.dataSource = self
//        self.enviarMensagem()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionFesta.collectionViewLayout.invalidateLayout()
    }

    func enviarMensagem(){
//        FIRMessaging.messaging().subscribe(toTopic: "eo4lxU6ylLg:APA91bHfEH3sCk6ua3Gdef5i5wlW8MlPXkX0QULAPAckx96ob2rIfBxIIdbEBkr8pk7SlrLoVY0k_6ukxeffkD-VwcE7tr5qElj68_arY-HAEndR6zd4BDbM3fEVDhZDg58AuLhoqatU")
//        var request = URLRequest(url: URL(string: "https://fcm.googleapis.com/fcm/send")!)
//        request.httpMethod = "POST"
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.setValue("key=AIzaSyCKxIgClQpL3Pm2DcT09xP7GuheQ1xixGU", forHTTPHeaderField: "Authorization")
//        guard let toke = FIRInstanceID.instanceID().token() else {
//            return
//        }
//        print(toke)
//        let json: [String : Any] = ["to": "cBj2YnwFlLY:APA91bF--IsNjmvact5U50ZU0PSXcyf7sMLT26GDbwWfaez0idueFAlxrdHkFwhjYzlglV9Z5-bxQGX5Spzb0FxKNU2QSDhDCSRVJMqTp1Ks0z2y7ATTySNZnJjONkA2bTRoYExXhJMf",
//                                    "notification": ["title": "Teste de mensagem device", "text" : "great match!"]]
//        do {
//            let d = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
//            request.httpBody = d
//            print(d)
//            URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
//                if let error = error {
//                    print(error)
//                }
//                do {
//                    if let j = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? Any{
//                    print(j)
//                    }
//                }catch {
//                }
//                print(data, response , error)
//            }.resume()
//        }catch {
//            
//        }
        
        FIRMessaging.messaging().sendMessage(["notification": ["title": "Teste de mensagem device", "text" : "great match!"]], to: "cBj2YnwFlLY:APA91bF--IsNjmvact5U50ZU0PSXcyf7sMLT26GDbwWfaez0idueFAlxrdHkFwhjYzlglV9Z5-bxQGX5Spzb0FxKNU2QSDhDCSRVJMqTp1Ks0z2y7ATTySNZnJjONkA2bTRoYExXhJMf", withMessageID: "101010", timeToLive: 100)
    }
    
    // Mark: - Firebase
    
    func fetchFesta(_ festaId: String) -> Void {
        FestaStore.singleton.fetchFesta(withId: festaId) { (festa: Festa?) in
            if let f = festa {
                self.festas.append(f)
                self.attemptReloadOfCollection()
            } else {
                self.unlock()
            }
        }
    }
    
    func fetchRemoveFesta() {
        FestaStore.singleton.fetchRemoveFesta { (key: String) in
            if self.festas.isEmpty {
                return
            }
            let index = self.festas.index(where: { (f: Festa) -> Bool in
                return f.id == key
            })
            if let i = index {
                self.festas.remove(at: i)
                DispatchQueue.main.async {
                    self.collectionFesta.deleteItems(at: [IndexPath(item: i, section: 0)])
                }
            }
        }
    }
    
    func observeUserFestas() -> Void {
        FestaStore.singleton.fetchFestas { (key: String) in
            self.lock()
            self.fetchFesta(key)
        }
    }
    
    private func imageLayerForGradientBackground() -> UIImage {
        var updatedFrame = self.navigationController?.navigationBar.bounds
        // take into account the status bar
        updatedFrame?.size.height += 20
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame!)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
//    func fetchEnquete(_ enqueteId: String) -> Void {
//        EnqueteStore.singleton.fetchEnquete(withId: enqueteId) { (enquete: Enquete?) in
//            if let e = enquete {
//                self.enquetes.append(e)
//                self.attemptReloadOfCollection()
//            }
//        }
//    }
//    
//    func observeEnquetesFesta() -> Void {
//        EnqueteStore.singleton.fetchEnquetes { (key: String) in
//            self.fetchEnquete(key)
//        }
//    }
    
    // Mark: - Collection
    
    var timer: Timer?
    private func attemptReloadOfCollection() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.handleReloadCollection), userInfo: nil, repeats: false)
    }
    
    func handleReloadCollection() {
        DispatchQueue.main.async {
            self.unlock()
            print(#function,"Collections de festas!")
            self.collectionFesta.reloadData()
        }
    }
    

    // Mark: - Login and Logout of user
    
    private func isUserLogged() {
        if UserStore.singleton.isUserLogged() {
            self.fetchUser()
        } else {
            self.perform(#selector(self.logoutPressed), with: nil, afterDelay: 0)
        }
    }
    
    func fetchUser(){
        UserStore.singleton.fetchUser { (user: User?) in
            if let user = user {
                self.setupProfile(with: user)
            }
        }
    }
    
    func setupProfile(with user: User) {
        self.festas.removeAll()
        self.collectionFesta.reloadData()
        if let name = UserStore.singleton.user?.nome {
            usernameLabel.text = name
        }
        
        if let url = UserStore.singleton.user?.imageProfileUrl {
            self.profileImage.loadImageUsingCache(withUrlString: url)
        }

        self.observeUserFestas()
        self.fetchRemoveFesta()
    }
    
    @IBAction func logoutPressed() {
        guard let loginController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
            return
        }
        loginController.partiesViewController = self
        loginController.userLogoutFacebook()
        self.present(loginController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToPartyView" {
            let checklistVC = segue.destination as! ChecklistViewController
            if let index = self.collectionFesta.indexPathsForSelectedItems?.first {
                checklistVC.festa = self.festas[index.item]
            }
        } else if segue.identifier == "convites"{
            let convites = segue.destination as! ConviteNotificationController
            convites.partiesController = self
        }
    }
    
    
}

extension PartiesViewController: UICollectionViewDelegate {
}

extension PartiesViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.festas.count
    }
    
    private func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedParty = self.festas[indexPath.row]
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! CellFeedFesta
        cell.festa = self.festas[indexPath.row]
        
        return cell
    }
}

extension PartiesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width - 20, height: 106)
    }
    
}
