//
//  PopoverItemController.swift
//  Partify
//
//  Created by Thiago Vinhote on 30/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import Firebase

class PopoverItemController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var buttonLevar: UIButton!
    @IBOutlet weak var container: UIView! {
        didSet{
            self.container.setShadow()
        }
    }
    
    @IBOutlet weak var nabBar: UINavigationBar!
    
    var contains: Bool? = false
    
    var users: [User] = []
    weak var item: Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.setup()
    }
    
    @IBAction func handleFechar(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func verificaButton() {
        self.contains = self.users.contains(where: { (user: User) -> Bool in
            return user.id == UserStore.singleton.user?.id
        })
        if let b = self.contains {
            self.buttonLevar.setTitle(b == true ? "Deixar" : "Levar", for: .normal)
        }
    }
    
    private func setup() {
        self.nabBar.setBackgroundImage(UIImage(), for: .default)
        if let idItem = self.item?.id {
            ItemStore.singleton.fetchItemUser(withId: idItem, nil, completion: { (user: User?, error: Error?) in
                if let e = error {
                    print(e)
                    return
                }
                if let u = user {
                    self.users.append(u)
                    if let index = self.users.index(of: u) {
                        self.tableView.insertRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                    }
                }
                self.verificaButton()
            })
            ItemStore.singleton.fetchRemovedItemUser(withId: idItem, nil, completion: { (user: User?, error: Error?) in
                if let e = error {
                    print(e)
                    return
                }
                if let index = self.users.index(where: { (usuario: User) -> Bool in
                    return user?.id == usuario.id
                }) {
                    self.users.remove(at: index)
                    self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                }
                self.verificaButton()
            })
        }
    }
    
    @IBOutlet weak var navItemTitle: UINavigationItem! {
        didSet{
            self.navItemTitle.title = self.item?.nome
        }
    }
    
    @IBAction func handlePegarItem(_ sender: Any) {
        if let c = self.contains {
            if !c {
                if let itemId = self.item?.id, let idUser = UserStore.singleton.user?.id {
                    ItemStore.singleton.addItemUser(withId: itemId, idUser: idUser, completion: { (error: Error?, ref: FIRDatabaseReference) in
                        if let e = error {
                            print(e)
                            return
                        }
                    })
                }
            } else {
                if let itemId = self.item?.id, let idUser = UserStore.singleton.user?.id {
                    ItemStore.singleton.removeItemUser(withId: itemId, idUser: idUser, completion: { (error: Error?, ref: FIRDatabaseReference) in
                        if let e = error {
                            print(e)
                            return
                        }
//                        if let index = self.cellItem?.attributedFriends.index(where: { (user: User) -> Bool in
//                            return user.id == UserStore.singleton.user?.id
//                        }) {
//                            self.cellItem?.attributedFriends.remove(at: index)
//                            self.cellItem?.collectionView.reloadData()
//                            
//                            if let indexTable = self.users?.index(where: { (user: User) -> Bool in
//                                return user.id == UserStore.singleton.user?.id
//                            }) {
//                                self.tableView.deleteRows(at: [IndexPath(row: indexTable, section: 0)], with: .fade)
//                            }
//                            
//                        }
                    })
                }
            }
        }
    }
    
}

extension PopoverItemController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath)
        cell.textLabel?.text = self.users[indexPath.item].nome
        return cell
    }
    
}
