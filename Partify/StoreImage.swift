//
//  StoreImage.swift
//  Partify
//
//  Created by Thiago Vinhote on 16/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit
import Firebase

class StoreImage: NSObject {
    
    static let singleton : StoreImage = StoreImage()
    
    private override init() {
        super.init()
    }
    
    func uploadToFirebaseStorage(using image: UIImage, completion: @escaping (_ imageUrl: String, _ error: Error?) -> Void) -> Void {
        let imageName = NSUUID().uuidString
        let ref = FIRStorage.storage().reference().child("grupo_images").child(imageName)
        if let uploadData = UIImageJPEGRepresentation(image, 0.1) {
            ref.put(uploadData, metadata: nil, completion: { (metadata: FIRStorageMetadata?, error: Error?) in
                if let e = error {
                    print(e)
                    completion("", e)
                    return
                }
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    completion(imageUrl, nil)
                }
            })
        }
    }

}
