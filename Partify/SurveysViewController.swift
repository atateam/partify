//
//  SurveysViewController.swift
//  Partify
//
//  Created by Arthur Melo on 17/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import UIKit

class SurveysViewController: UIViewController {

    @IBOutlet weak var surveyName: UITextField!
    @IBOutlet weak var surveyQuestion: UITextField!
    @IBOutlet weak var surveyDate: UIDatePicker!
    
    var enquetes: [Enquete] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func savePressed(_ sender: UIBarButtonItem) {
        let enquete = Enquete()
        enquete.nome = self.surveyName.text
        enquete.pergunta = self.surveyQuestion.text
        enquete.data = Int(self.surveyDate.date.timeIntervalSince1970) as NSNumber
        
        EnqueteStore.singleton.createEnquete(enquete) { (enquete: Enquete?, error: Error?) in
            if error != nil {
                print("Aconteceu algo de errado ao tentar salvar!", error!)
                return
            }
            if enquete != nil {
                //EnqueteStore.singleton.createFestaEnquete(idFesta: <#T##String#>, <#T##enquete: Enquete##Enquete#>)
            }
            _ = self.navigationController?.popViewController(animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
