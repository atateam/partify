//
//  User.swift
//  Partify
//
//  Created by Thiago Vinhote on 12/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import Foundation

class User: NSObject {
    
    var id: String?
    var nome: String?
    var email: String?
    var imageProfileUrl: String?
    var tokenMessaging: String?
    
    convenience init(dic: [String: Any]) {
        self.init()
        self.setValuesForKeys(dic)
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if self.responds(to: NSSelectorFromString(key)) {
            super.setValue(value, forKey: key)
        }
    }
    
    override var description: String {
        return "\(self.nome) \(self.email)"
    }
}
