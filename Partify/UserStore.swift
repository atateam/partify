//
//  UserStore.swift
//  Partify
//
//  Created by Thiago Vinhote on 12/11/16.
//  Copyright © 2016 ATA Team. All rights reserved.
//

import Foundation
import Firebase
import FBSDKLoginKit

class UserStore: NSObject {
    
    static let singleton : UserStore = UserStore()
    var user: User?
    
    private override init() {
        super.init()
        if let userId = FBSDKAccessToken.current()?.userID {
            self.fetchUser(withId: userId) { (user: User?) in
                if let user = user {
                    self.user = user
                }
            }
        }
    }
    
    private let usersReference : FIRDatabaseReference = {
        return FIRDatabase.database().reference().child("users")
    }()
    
    func createUser(_ user: User, completion: ((_ user: User?, _ error: Error?) -> Void)?) {
        guard let id = user.id else {
            completion?(nil, nil)
            return
        }
        let email = user.email
        let token = user.tokenMessaging
        guard let nome = user.nome, let imageProfileUrl = user.imageProfileUrl else {
            completion?(nil, nil)
            return
        }
        let userDic: [String: Any] = ["nome": nome, "email": email, "imageProfileUrl": imageProfileUrl, "tokenMessaging": token]
        let childUser = usersReference.child(id)
        childUser.updateChildValues(userDic) { (error: Error?, ref: FIRDatabaseReference) in
            if let e = error {
                print(e)
                completion?(nil, e)
                return
            }
            completion?(user, nil)
        }
    }
    
    func userLogout() {
        do {
            let manager = FBSDKLoginManager()
            manager.logOut()
            try FIRAuth.auth()?.signOut()
        } catch let error {
            print(error)
        }
    }
    
    func isUserLogged() -> Bool {
        return FBSDKAccessToken.current()?.userID != nil
    }
    
    func fetchUser(withId id: String? = nil, completion: @escaping (_ user: User?) -> Void) {
        guard let uid = id ?? FBSDKAccessToken.current()?.userID else {
            completion(nil)
            return
        }
        
        usersReference.child(uid).observeSingleEvent(of: .value, with: { (snapshot: FIRDataSnapshot) in
            if let dictionary = snapshot.value as? [String: Any] {
                let user = User(dic: dictionary)
                user.id = snapshot.key
                completion(user)
            }
            completion(nil)
            
        }, withCancel: nil);
    }
    
    func singIn(withToken token: String, completion: @escaping (_ user: User?, _ error: Error?) -> Void) {
        
        let credencial = FIRFacebookAuthProvider.credential(withAccessToken: token)
        FIRAuth.auth()?.signIn(with: credencial, completion: { (user: FIRUser?, error:Error?) in
            if error != nil{
                completion(nil, error)
                return
            }
            let usuario = User()
//            usuario.id = user?.uid
            usuario.id = FBSDKAccessToken.current().userID
            self.user = usuario
            completion(usuario, nil)
        })
        
    }
    
    func graphRequestFacebook(completion: @escaping (_ result: Any?, _ error: Error?) -> Void) {
        let parameters = ["fields":"email, first_name, last_name, picture.type(large)"]
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: { (connection: FBSDKGraphRequestConnection?, result: Any?, error: Error?) in
            if let e = error{
                completion(nil, e)
                return
            }
            completion(result, nil)
        })
    }
    
    func graphRequestFacebookFriends(completion: @escaping (_ friends: [Friend]?, _ error: Error?) -> Void) -> Void {
        let parameters = ["fields":"email, name, id, birthday"]
        FBSDKGraphRequest(graphPath: "me/friends", parameters: parameters).start { (connection: FBSDKGraphRequestConnection?, result: Any?, error: Error?) in
            if let e = error {
                completion(nil, e)
                return
            }
            var friends: [Friend] = []
            if let dic = result as? NSDictionary {
                print(dic)
                if let data = dic.object(forKey: "data") as? [[String: Any]] {
                    data.forEach({ (dicFriend: [String : Any]) in
                        let friend = Friend(dic: dicFriend)
                        friends.append(friend)
                    })
                }
            }
            completion(friends, nil)
        }
    }
    
    func graphRequestFacebook(id: String, completion: @escaping (_ result: Any?, _ error: Error?) -> Void) {
        let parameters = ["fields":"email, name,first_name, last_name, installed, work, id, birthday, picture.type(large)"]
        FBSDKGraphRequest(graphPath: "/\(id)", parameters: parameters).start(completionHandler: { (connection: FBSDKGraphRequestConnection?, result: Any?, error: Error?) in
            if let e = error{
                completion(nil, e)
                return
            }
            completion(result, nil)
        })
    }
}
